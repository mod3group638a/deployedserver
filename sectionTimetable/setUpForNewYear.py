import sys
from openpyxl import *
import json
sectionsLeft = True

sectionNum = 1 # set to 0 when done testing

while sectionsLeft:
    
    if sectionNum < 10:
        name = "Section0" + str(sectionNum) + "Timetable.xlsx"
    else:
        name = "Section" + str(sectionNum) + "Timetable.xlsx"
    print(name)
    
    #try:
    wb = load_workbook(name) 
    #except IOError:
    sectionsLeft = False
    #break

    counter = 0
    

    data = []

    print(wb.sheetnames)
    notFirstLine = False
    for row in wb['Sheet1']:
        if notFirstLine:
            if row[5].value != None:
                data.append({
                    'day' : row[0].value,
                    'start': str(row[1].value),
                    'end': str(row[2].value),
                    'type': row[4].value,
                    'course': row[3].value[4:],
                    'note': row[5].value
                })
            else:
                data.append({
                    'day' : row[0].value,
                    'start': str(row[1].value),
                    'end': str(row[2].value),
                    'type': row[4].value,
                    'course': row[3].value[4:]
                })
            counter += 1
        notFirstLine = True
    #data['length'] = counter+1
    with open('../public/section'+str(sectionNum)+'.owen', 'w') as outfile:
        json.dump(data, outfile)
    print(sectionNum)
    sectionNum += 1