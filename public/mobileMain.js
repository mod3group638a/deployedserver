class Coordinate { //coordinate class If you don't know OOP(object orientated Programming) please learn this or ask Owen
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.hash = coordsHash(x, y); //cantor pairing function, thanks wikipedia --- generates unique int based on 2 inputs
    }
    startTime() {
        return `${Math.trunc((this.y * 15) / 60)}${Math.trunc((this.y * 15) % 60)}00`; //gets a time in 24 hour time from the coordinate
    }
    endTime() {
        return `${Math.trunc(((this.y + 1) * 15) / 60)}${Math.trunc(((1 + this.y) * 15) % 60)}00`;//gets a time in 24 hour time from the coordinate
    }
}
class Event { //another class this one basically just holds information
    constructor(code, modifier, type, startTime, endTime, startTimeStr, endTimeStr, day, isClass, colour) {
        this.code = code;
        this.name = code;
        this.modifier = modifier;
        this.type = type;
        this.startTime = startTime;
        this.endTime = endTime;
        this.startTimeStr = startTimeStr;
        this.endTimeStr = endTimeStr;
        this.day = day;
        this.visible = false;
        this.startCoords = new Coordinate(day, startTime);
        this.endCoords = new Coordinate(day, endTime);
        this.isClass = isClass;
        this.colour = colour;
    }

    //------------------------------------------------------- add time to time string function ------------------------------

    isSameType(event) { //checks if this event is the same as another
        if (event.code !== this.code) return false;
        if (event.modifier !== this.modifier) return false;
        if (event.type !== this.type) return false;
        if (event.name !== this.name) return false;
        return true;
    }
    changeEndCoords(coords) { //changes the end cordinates of an event object
        this.endCoords = coords;
        this.endTime = this.endCoords.y;
        this.endTimeStr = getTimeString(coords.y);
    }
}
class SelectedArea { //class for selected area again just for holding info
    constructor(coords, div) {
        this.startCoords = coords;
        this.endCoords = getEndCoords(coords);
        this.div = div;
    }
}

var data = []; //array of all scheduled events
var course = []; //list of schedulable activities WIP
var titles = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]; //list of colum titles
var hoursPerCourse = [4, 2, 3, 5, 6, 2, 4, 0, 0, 0]; //array of suggested number of hours per course
var selected = []; //array of all selected areas
var numCourseSelected = []; // array of number of each course selected
var hourMulti = 60; //height multiplier for boxes 1 hour = hourMulti pixels
var hoursBoxes = []; //the number of selected hours per course
var colours = ["#FFEF00", "#ff8533", "#cc00ff", "#A2E4FC", "#6AF647", "#FF69D8", "#0CFFF8", "#ff9966", "#66ccff", "#00ff99", "white"]; //colours array. will need more colours
var codeToColourArray = []; //links code inded to colour index
var numSelected = 0; //number of selected boxes, depreceated
var offset = hourMulti / 4 * 3
var customCodes = new Array(); //array of all custom codes
var customMaxCode = 1000; // the next custom code to be assigned
var colourCounter = 0; // the last used colour index
var date = new Date(); //the current date
var occupiedLocation = []; //array of all occupied loactions
//end of start variables 
var isBySection = false;
var displayedDay = 0; //0 = monday this is the currently displayed day
var requested = false;
var selectedTextBox = document.createTextNode("numSelec"); //box for showing the number of selected items

var secSel = document.getElementById("secSelect"); //createes drop down list for the section numbers

var mouseX; //x position of mouse
var mouseY; //y position of mouse
var doubleTapped = false;
var mylatesttap;
var stage = 0;

var top = document.getElementById("calendar");
var onGoingTouches = [];
top.addEventListener("touchstart", function (evt) {
    let touches = evt.changedTouches;
    for (let i = 0; i < touches.length; i++) {
        let idx = ongoingTouchIndexById(touches[i].identifier);
        if (idx >= 0) {
            onGoingTouches.splice(idx, 1, copyTouch(touches[i]));
        } else {
            onGoingTouches.push(copyTouch(touches[i]));
        }
    }
    updateSelected(evt);
}, false);
top.addEventListener("touchend", function (evt) {
    let touches = evt.changedTouches;
    console.log(touches);
    for (let i = 0; i < touches.length; i++) {
        let idx = ongoingTouchIndexById(touches[i].identifier);
        if (idx >= 0) {
            onGoingTouches.splice(idx, 1);
        }
    }
    updateSelected(evt);
}, false);
top.addEventListener("touchcancel", function (evt) {
    let touches = evt.changedTouches;
    for (let i = 0; i < touches.length; i++) {
        let idx = ongoingTouchIndexById(touches[i].identifier);
        if (idx >= 0) {
            onGoingTouches.splice(idx, 1);
        }
    }
    updateSelected(evt);
}, false);
top.addEventListener("touchmove", function (evt) {
    let touches = evt.changedTouches;
    for (let i = 0; i < touches.length; i++) {
        let idx = ongoingTouchIndexById(touches[i].identifier);
        if (idx >= 0) {
            onGoingTouches.splice(idx, 1, copyTouch(touches[i]));
        }
    }
    updateSelected(evt);
}, false);
function copyTouch(touch) {
    let rect = touch.target.getBoundingClientRect();
    return { identifier: touch.identifier, pageX: touch.clientX - rect.left, pageY: touch.clientY - rect.top, target:touch.target.id};
}

function ongoingTouchIndexById(idToFind) {
    for (let i = 0; i < onGoingTouches.length; i++) {
        let id = onGoingTouches[i].identifier;

        if (id == idToFind)
            return i;


    }
    return -1;    // not found
}
function updateSelected(evt) {
    console.log(onGoingTouches);
    console.log(onGoingTouches[0].pageY);
    if (onGoingTouches.length > 1 && onGoingTouches[0].target == "calendar" && onGoingTouches[1].target == "calendar") {
        evt.preventDefault();
        let top = Math.trunc(onGoingTouches[0].pageY / (hourMulti / 4));
        let bottom = Math.trunc(onGoingTouches[1].pageY / (hourMulti / 4));
        //document.getElementById("top").style.backgroundColor = "blue";
        if (top < bottom)
            for (let i = top; i < bottom; i++) {
                selectArea(i);

            }
        else
            for (let i = bottom; i < top; i++) {
                selectArea(i);
            }
    } else if (onGoingTouches.length == 1 && onGoingTouches[0].target == "calendar") {
        doubleClick(onGoingTouches[0]);
    }
}

function doubleClick(touch){
    var now = new Date().getTime();
    var timesince = now - mylatesttap;
    mylatesttap = now;
    if ((timesince < 600) && (timesince > 0)) {

        //document.getElementById("top").style.backgroundColor = "green";
        let top = Math.trunc(touch.pageY / (hourMulti / 4));
        selectArea(top);
    } else {
        return;
    }

}

document.getElementById("top").style.display = "none";
document.getElementById("bottom").style.display = "none";

for (let index = 0; index < 19; index++) {

    secSel.innerHTML += `<option>${index}</option>`;
}

var request = new XMLHttpRequest();
request.responseType = 'text'; //we are getting a text file
request.onload = function () { //when wwe recieve the file do this
    let loadedData = JSON.parse(request.response); //turn the text from the file into an object using JSON
    for (let index = 0; index < loadedData.length; index++) { //iterate through recieved data
        const cT = loadedData[index];
        let code = parseInt(cT.course);
        //console.log(code);
        if (codeToColourArray[code] == null) {
            codeToColourArray[code] = colourCounter;
            colourCounter++;
        }
        if (course.indexOf(code) < 0) course.push(code);
        let day; //turn the words for days of the week into a number
        switch (cT.day) {
            case "Mon":
                day = 0;
                break;
            case "Tue":
                day = 1;
                break;
            case "Wed":
                day = 2;
                break;
            case "Thu":
                day = 3;
                break;
            case "Fri":
                day = 4;
                break;
            case "Sat":
                day = 5;
                break;
            case "Sun":
                day = 6;
                break;
        }
        day += 1;
        let startTime = cT.start.replace(":", "");//replace the 2 : from the start and end time
        startTime = startTime.replace(":", "");
        let endTime = cT.end.replace(":", "");
        endTime = endTime.replace(":", "");
        endTime = parseInt(endTime);//turn the string into an int
        startTime = parseInt(startTime);
        if (startTime < 83000) { //takes from 12 hour time and turns into number of spaces from the top of the calendar
            startTime += 120000;
            startTime -= 73000;
            startTime /= 10000;
        } else {
            startTime -= 73000;
            startTime /= 10000;
        }
        if (endTime < 93000) {
            endTime += 120000;
            endTime -= 73000;
            endTime /= 10000;
        } else {
            endTime -= 73000;
            endTime /= 10000;
        }

        //adds the new event Object to the data array at an array location based on the start and end time of the event
        //console.log( colours[codeToColourArray[code]] + "     " +  codeToColourArray[code]);
        data[(new Coordinate(day, startTime * 4).hash)] = new Event("APSC " + code, cT.note, cT.type, startTime * 4, endTime * 4, cT.start, cT.end, day, true, colours[codeToColourArray[code]]);
        isBySection = true;
    }

    generateCalendar(); //once all data is loaded generate the calendar 
}

function getTimeString(num) {
    let hours, mins;
    hours = Math.trunc((num + 2) / 4) + 7;
    if (hours > 12) hours -= 12;

    let temp = (2 + num) / 4 * 100;
    temp = temp % 100;
    mins = temp / 100 * 60;

    if (mins != 0)
        return hours + ":" + mins;
    else
        return hours + ":" + mins + "0";
}

function createCalendar() { //is run when user selects a section number
    request.open('GET', "section" + secSel.value + ".owen"); //start the request for section schedule data 
    request.send(); //sends the request for schedule data
}

function generateCalendar() { //gets run when request is finished parsing
    console.log(data);
    document.getElementById("top").style.display = "";//"inline-block"; //show the calendar area
    document.getElementById("bottom").style.display = "";//"inline-block"; //show the select course area
    document.getElementById("getStarted").style.display = "none"; //hide the login area
    stage++;
    occupiedLocation = [];
    data.forEach(function (value) {
        if (value != null)
            showEvent(value);
    }); //for every event in data run the show event function to add event to screen

    selectedTextBox.nodeValue = "Number of hours selected: " + numSelected; //displays the number of selected courses
    document.getElementById("bottom").appendChild(selectedTextBox); //adds displayed number to the select course section

    var timeAndColourTable = document.getElementById("timeAndColourTable"); //gets reference to the select course table
    timeAndColourTable.insertRow(); //creates top row
    //adds heading to the table
    //console.log(course);
    let td = document.createElement("TD"); //create new cell
    let tn = document.createTextNode("Course"); //create text to put in cell
    td.appendChild(tn); //add text to cell
    timeAndColourTable.rows[0].appendChild(td); // adds cell to table

    td = document.createElement("TD"); //create new cell
    tn = document.createTextNode("# Hours"); //create text to put in cell
    td.appendChild(tn); //add text to cell
    timeAndColourTable.rows[0].appendChild(td); // adds cell to table
    if (isBySection) {
        td = document.createElement("TD"); //create new cell
        tn = document.createTextNode("# Hours Required"); //create text to put in cell
        td.appendChild(tn); //add text to cell
        timeAndColourTable.rows[0].appendChild(td); // adds cell to table
    }

    for (let index = 0; index < course.length; index++) {
        timeAndColourTable.insertRow(); //adds row to table
        let td = document.createElement("TD"); //creates a cell in the table
        td.style.backgroundColor = colours[codeToColourArray[course[index]]]; //sets the cell to be the correct colour
        let tn = document.createTextNode(course[index]); //puts the course name in the cell
        td.name = course[index]; //sets the name of the HTML element to be the current index for referencing later
        td.addEventListener('click', function () { //when the box is clicked
            setCourses(this.name); //set all selected boxes to this boxes course code
        }, false);
        td.appendChild(tn); //add text to cell
        timeAndColourTable.rows[index + 1].appendChild(td); //add cell to table
        td = document.createElement("TD"); //create new cell
        tn = document.createTextNode("0"); //create text to put in cell
        td.appendChild(tn); //add text to cell
        timeAndColourTable.rows[index + 1].appendChild(td); // adds cell to table
        hoursBoxes[index] = td; //adds the text box to array of text boxes for referencing later
        if (isBySection) {
            td = document.createElement("TD"); //creates cell
            tn = document.createTextNode(hoursPerCourse[index]); // creates text for cell
            td.appendChild(tn); //adds text to cell
            timeAndColourTable.rows[index + 1].appendChild(td); //adds cell to table
        }
    }
    let calendar = document.getElementById("calendar");
    let timeDiv = document.getElementById("times");
    titles.forEach(function (value) {
        let div = document.getElementById(value);
        for (let index = 0; index < 17; index++) {
            let time = index + 8;
            if (time > 12) time -= 12;
            div.innerHTML += `<hr class="hour-line" style="top:${index * hourMulti + hourMulti / 2}px; width:100%" data-hour="${index + 7}:30"> </hr>`
            //times.innerHTML += `<div class = "hour-nums" style="top:${index * hourMulti + hourMulti / 2 * 3}px;"> ${time}:30 </div>`;
        }
    });

    updateShowedDay();

}

function startDL() { //gets run when user clicks the download button

    let text = data.reduce(function (text, value) { //array iterator function 
        if (value != null)
            return text + createVEVENT(value.startCoords.startTime(), value.endCoords.endTime(), value.name, value.startCoords.x, value.colour); //calls function to get event in VCAL format
        else return text;
    });
    download("BEGIN:VCALENDAR\nVERSION:2.0" + text + "\nEND:VCALENDAR", "timeTable.ics"); //calls download function with the data and file name
}

function createVEVENT(startTime, endTime, name, dayOfWeek, colour) {//day 0 = monday currently only works for SEM 1 2019 will need to change, not commenting since it will change
    let date = dayOfWeek + 2;
    let startTimeS = "" + startTime;
    let endTimeS = "" + endTime;
    if (endTime < 100000) {
        endTimeS = "0" + endTime;
    }
    if (startTime < 100000) {
        startTimeS = "0" + startTime;
    }
    let endMonth = 5;
    if (date.getMonth() > 7) endMonth = 12;

    return "\nBEGIN:VEVENT\nSUMMARY:" + name + "\nDTSTART:201909" + date + "T" + startTimeS + "\nDTEND:201909" + date + "T" + endTimeS + "\nCOLOR:" + colour + `\nRRULE:FREQ=WEEKLY;UNTIL=${date.getFullYear()}${endMonth}25T000000\nEND:VEVENT`;


}
function download(data, filename, type) {
    var file = new Blob([data], { type: type }); //creates a "file"
    if (window.navigator.msSaveOrOpenBlob) // IE10+ 
        window.navigator.msSaveOrOpenBlob(file, filename); //prompt internet explorer users to save or open the file
    else { // Others
        var a = document.createElement("a"), //creates HTML element to attach save function to 
            url = URL.createObjectURL(file); // creates a URL for the blob
        a.href = url;
        a.download = filename; //sets the download name to the file name
        document.body.appendChild(a); // adds HTML element to screen
        a.click(); //clicks on the HTML element to staart the download
        setTimeout(function () { //if the user does nothing for too long
            document.body.removeChild(a); //get rid of download screen
            window.URL.revokeObjectURL(url); //get rid of URL 
        }, 0);
    }
}


function update() {
    selectedTextBox.nodeValue = "Number of hours selected: " + (numSelected / 4);
    for (let index = 0; index < course.length; index++) {
        numCourseSelected[index] = 0;
    }
    data.forEach(function (value) {
        if (value != null) {
            if (!value.isClass) {
                numCourseSelected[codeToColourArray[value.code]]++;
            }
        }
    });
    for (let index = 0; index < hoursBoxes.length; index++) {
        hoursBoxes[index].innerHTML = numCourseSelected[index] + "";
    }
}

function setCourses(selectedCourseNumber) {
    selected.forEach(function (value, index, array) { //for every element in the array
        if (value != null) {
            let filtered = array.filter(function (value2) {
                if (value2 != null)
                    return value.startCoords.hash === value2.endCoords.hash; //checks if 2 selected zones are touching
            });
            if (filtered[0]) { // if they were touching then change the old end coord to the new end coord
                filtered[0].endCoords = value.endCoords;
                ////console.log(value);
                let tempDiv = document.getElementById(`${value.startCoords.hash}`);
                tempDiv.parentNode.removeChild(tempDiv); //remove the old selected box
                array[index] = null;
            }
        }
    });
    data.forEach(function (value) { //hide all events
        if (value != null)
            hideEvent(value);
    });
    selected.forEach(function (value, index, array) {
        if (value != null) {
            //console.log(value);
            let event = createEventObj(selectedCourseNumber, value.startCoords, value.endCoords, colours[codeToColourArray[selectedCourseNumber]], selectedCourseNumber + ' STUDY TIME'); //create event for all selected areas
            data[event.startCoords.hash] = event; //adds event to data array 
            let tempDiv = document.getElementById(`${value.startCoords.hash}`);
            tempDiv.parentNode.removeChild(tempDiv); //remove the old selected box
            array[index] = null;
        }
    });
    selected = [];
    //console.log(selected);

    data.forEach(function (value, index, array) { //same as function above but for events not selected areas and also makes sure the 2 events are the same
        if (value != null) {
            let filtered = array.filter(function (value2) {
                if (value2 != null)
                    return value.isSameType(value2) && coordsHash(value.startCoords.x, value.startCoords.y /*- 1*/) === value2.endCoords.hash;
            });
            if (filtered[0]) {
                filtered[0].changeEndCoords(value.endCoords);
                array[index] = null;
            }
        }
    });
    occupiedLocation = [];
    data.forEach(function (value) { //show all events again
        if (value != null)
            showEvent(value);
    });
    numSelected = 0; //there are no longer any boxes selected
    update(); //updates time and colour table
}

function deselectAll() { // TODO fix this to work with occupied areas
    selected.forEach(function (value, index, array) { //array iterator function to remove all red boxes
        document.getElementById(titles[value.startCoords.x]).removeChild(value.div);
        array[index] = null;
    });
    selected = [];
    numSelected = 0;
    update();
}

function deselect(area) { //gets run if a selected area is clicked removes selected area object
    selected.forEach(function (value, index, array) {
        let tempDiv = document.getElementById(`${value.startCoords.hash}`);
        tempDiv.parentNode.removeChild(tempDiv); //remove the old selected box
        array[index] = null;
    });
    selected = [];
}

function custom() {  //gets called when user creates a custom event
    let text = document.getElementById("custom").value; //gets the custom text
    if (customCodes[text] == null) {
        customCodes[text] = customMaxCode;
        customMaxCode++;
    }
    if (codeToColourArray[customCodes[text]] == null) {
        codeToColourArray[customCodes[text]] = colourCounter;
        colourCounter++;
    }
    selected.forEach(function (value, index, array) { //for every element in the array
        let filtered = array.filter(function (value2) {
            if (value2 != null)
                return value.startCoords.hash === value2.endCoords.hash; //checks if 2 selected zones are touching
        });
        if (filtered[0]) { // if they were touching then change the old end coord to the new end coord
            filtered[0].endCoords = value.endCoords;
            ////console.log(value);
            let tempDiv = document.getElementById(`${value.startCoords.hash}`);
            tempDiv.parentNode.removeChild(tempDiv); //remove the old selected box
            array[index] = null;
        }
    });
    data.forEach(function (value) { //hide all events
        if (value != null)
            hideEvent(value);
    });
    selected.forEach(function (value, index, array) {
        if (value != null) {
            //console.log(value);
            let event = createEventObj(customCodes[text], value.startCoords, value.endCoords, colours[codeToColourArray[customCodes[text]]], text); //create event for all selected areas
            if (data[event.startCoords.hash] == null) data[event.startCoords.hash] = event; //adds event to data array 
            let tempDiv = document.getElementById(`${value.startCoords.hash}`);
            tempDiv.parentNode.removeChild(tempDiv); //remove the old selected box
            array[index] = null;
        }
    });
    selected = [];
    //console.log(selected);
    if (data[0])
        data.forEach(function (value, index, array) { //same as function above but for events not selected areas and also makes sure the 2 events are the same
            if (value != null) {
                let filtered = array.filter(function (value2) {
                    return value.isSameType(value2) && coordsHash(value.startCoords.x, value.startCoords.y - 1) === value2.endCoords.hash;
                });
                if (filtered[0]) {
                    filtered[0].changeEndCoords(value.endCoords);
                    array[index] = null;
                }
            }
        });
    occupiedLocation = [];
    data.forEach(function (value) { //show all events again
        if (value != null)
            showEvent(value);
    });
    numSelected = 0; //there are no longer any boxes selected
    update(); //updates time and colour table
    console.log(document.getElementsByClassName("scheduled_event"));

}

function selectArea() { // TODO figure out how this works

}

function coordsHash(x, y) { //function for the fancy math used to determine a unique id based on a coordinate point
    return (((x + y) * (x + y + 1)) / 2) + y;
}
function showEvent(event) { //shows an event 
    if (event.visible === false) { //if the event isn't alreay showing
        let length = event.endTime - event.startTime;
        for (let index = event.startTime; index < length + event.startTime; index++) {
            occupiedLocation[coordsHash(event.day, index)] = true;

        }
        ////console.log(event);
        const dayDiv = document.getElementById(titles[event.day - 1]); //get ref to HTML element for the column
        if (event.isClass) {
            dayDiv.innerHTML = dayDiv.innerHTML + (`
            <div  style="top: ${ (event.startTime * hourMulti / 4) + offset}px; 
                min-height: ${(event.endTime * hourMulti / 4) - (event.startTime * hourMulti / 4)}px; background:${event.colour};" class="scheduled_event" id="${event.startCoords.hash}" name="${event.startCoords.hash}" ></div> `);
            //more jQuery again just ask owen or look it up. just makes the event appear onscreen
        } else {
            dayDiv.innerHTML = dayDiv.innerHTML + (`
            <div  style="top: ${ (event.startTime * hourMulti / 4) + offset}px; 
                min-height: ${(event.endTime * hourMulti / 4) - (event.startTime * hourMulti / 4)}px; background:${event.colour};" class="scheduled_event" id="${event.startCoords.hash}" name="${event.startCoords.hash}" onclick="removeEvent(id)"></div> `);

        }
        let eventDiv = document.getElementById(`${event.startCoords.hash}`); //gets ref to event area HTML element
        //styling stuff to make name and time look nice
        let className = document.createElement("div");
        className.className = "class_name";
        className.appendChild(document.createTextNode(event.name));
        eventDiv.appendChild(className);
        let classTime = document.createElement("div");
        classTime.className = "class_time";
        classTime.appendChild(document.createTextNode(event.startTimeStr + " -  " + event.endTimeStr));
        eventDiv.appendChild(classTime);
        event.visible = true;
    }
}

function hideEvent(event) { //removes event
    if (event.visible) {
        ////console.log(event);
        event.visible = false;
        document.getElementById(titles[event.day - 1]).removeChild(document.getElementById(`${event.startCoords.hash}`));
    }
}

function createEventObj(code, startCoords, endCoords, colour, name, isClass) {
    let temp = new Event(code);
    temp.code = code;
    temp.name = name;
    if (!isClass) {
        temp.modifier = "study_time";
        temp.type = "study_time";
    }
    temp.isClass = isClass;
    temp.startTime = startCoords.y;
    temp.endTime = endCoords.y;
    temp.day = startCoords.x;
    temp.startTimeStr = getTimeString(startCoords.y);
    temp.endTimeStr = getTimeString(endCoords.y);
    temp.visible = false;
    temp.startCoords = startCoords;
    temp.endCoords = endCoords;
    temp.colour = colour;
    return temp;
}

function getEndCoords(coords) {
    return new Coordinate(coords.x, coords.y + 1);
}

function removeEvent(id) {
    let event = data[id];
    delete data[id];
    let div = document.getElementById(id);
    div.parentNode.removeChild(div);
    let length = event.endTime - event.startTime;
    for (let index = event.startTime; index < length + event.startTime; index++) {
        occupiedLocation[coordsHash(event.day, index)] = false;

    }
}

function removeSelected(event) {
    delete selected[event];
    console.log(event);
    let div = document.getElementById(event);
    div.parentNode.removeChild(div);
}

function startSave() {
    let stu_num = document.getElementById("stu_num_save").value;
    console.log(stu_num);
    $.ajax({
        type: "POST",
        url: "/saveUSRDATA",
        data: { "stu_num": stu_num, "data": JSON.stringify(data) },
        success: function (response) {
            console.log(response);
        }
    });
}

function getData() {
    if (!requested) {
        requested = true;
        let stu_num = document.getElementById("stuNum").value;
        console.log("getting data");
        console.log(stu_num);
        $.ajax({
            type: "POST",
            url: "/getUSRDATA",
            data: { "stu_num": stu_num },
            success: function (response) {
                if (response == "Please Try A different Student Number") {
                    let temp = document.getElementById("errorMsg");
                    //console.log(temp);
                    temp.style.display = "";
                    requested = false;
                    return;
                }

                text = JSON.parse(response);
                //console.log(JSON.parse(text['data']));
                data = JSON.parse(text['data']);
                console.log(data);
                data.forEach(function (value) {
                    if (value != null) {
                        value.visible = false;
                        if (value.code >= 1000 && value.code > customMaxCode) customMaxCode = value.code;
                        if (value.code >= 1000 && customCodes[value.name] == null) {
                            customCodes[value.name] = value.code;
                        }
                        let colourNum = colours.indexOf(value.colour);
                        codeToColourArray[value.code] = colourNum;
                        if (course.indexOf(value.code) < 0 && value.code < 1000) course.push(value.code);

                    }
                });
                console.log(codeToColourArray);
                colourCounter = codeToColourArray.reduce(function (total, value) {
                    if (value > total) return value;
                    else return total;
                }, 0);
                colourCounter++;
                generateCalendar();
            }

        });
    }
}


document.getElementById('file').addEventListener('change', function (evt) {
    let files = evt.target.files;
    let file = files[0];
    let reader = new FileReader();
    reader.onload = function (event) {
        //console.log(event.target.result)
        let allData = ICAL.parse(event.target.result)[2];
        console.log(allData);
        let calData = allData.filter(function (value) {
            return value[0] == "vevent";
        }).map(function (value) {
            return value[1];
        });
        console.log(new Date(calData[0][4][3]).getHours());
        calData.forEach(function (value, index, array) {
            let startDate = new Date(value[4][3]);
            let day = startDate.getDay() - 1;
            if (day < 0) day = 6;
            day++;
            let startCoords = new Coordinate(day, (startDate.getHours() - 7.5) * 4 + startDate.getMinutes() / 15);
            let endDate = new Date(value[5][3]);
            let endCoords = new Coordinate(day, (endDate.getHours() - 7.5) * 4 + endDate.getMinutes() / 15);
            let summary = value[2][3];
            let code = value[2][3].substr(0, 8);
            if (codeToColourArray[code] == null) {
                codeToColourArray[code] = colourCounter;
                colourCounter++;
            }
            if (course.indexOf(code) < 0) course.push(code);
            data[startCoords.hash] = createEventObj(code, startCoords, endCoords, colours[codeToColourArray[code]], code, true);
        });
        console.log(data);
        generateCalendar();
    }
    reader.readAsText(file);
}, false);

function shiftLeft() {
    if (displayedDay - 1 >= 0) displayedDay--;
    updateShowedDay();
}

function shiftRight() {
    if (displayedDay + 1 <= 6) displayedDay++;
    updateShowedDay();
}
function updateShowedDay() {
    titles.forEach(function (value) {
        document.getElementById(value).style.display = "none";
    });
    document.getElementById(titles[displayedDay]).style.display = "";
    document.getElementById("header").innerHTML = `<h4>${titles[displayedDay]}</h4>`;
    console.log(titles[displayedDay]);
}

function selectArea(y) {


    let colNum = displayedDay + 1; //truncates and finds column number using bitwise operator
    if (colNum === 0) {
        //console.log("colnum 0");
        return; //if the user clicks on a time area this is pointless and user is just dumb
    }
    let rowNum = y;

    let coords = new Coordinate(colNum, rowNum); // creates coordinates object for the clicked location

    if (selected[coords.hash] != null || occupiedLocation[coords.hash] == true) {
        return; //if this has already been selected, return
    }
    let colDiv = document.getElementById(titles[colNum - 1]); //get the HTML refrence to the column
    colDiv.innerHTML += (`<div style="top:${rowNum * (hourMulti / 4) + offset}px; height:${(rowNum + 1) * (hourMulti / 4) - rowNum * (hourMulti / 4)}px; background:red;" id=${coords.hash} class="selected_area" name="${coords.hash}" onclick="removeSelected(id)"></div>`);//jQuery to get a red box to show up 
    //if you want to understand jQuery ask Owen he can explain
    selected[coords.hash] = new SelectedArea(coords, document.getElementById(`${coords.hash}`)); //little more jQuery to add selected zone to selected array
    numSelected++;
    update();
}


